# Introduction

*This is the repository for my Diploma Thesis.*

**Title**: Speech Denoising by Means of Deep Learning  
**Institution**: School of Electrical & Computer Engineering - Aristotle University of Thessaloniki, Greece  
**Author**: [Apostolos Morogiannis](https://gitlab.com/amorogia)  

## Abstract

This diploma thesis presents a deep learning and digital signal processing system for speech denoising. The system’s aim is to filter the noise contaminated speech signal. A four layer recurrent neural network consisting of gated recurrent units (GRU) and dense layers is used to predict ideal band gains, evenly spaced in the Mel scale. Different variations which use different transformations and input features for the aforementioned network are investigated and compared. At first, an existing baseline system (FourierRNN) is developed in which the Fourier transform is used as filtering and feature extraction mechanism; the Mel - frequency cepstral coefficients (MFCC) are used as input features in order to predict the ideal band gains, which in turn filter the magnitude of the noisy signal. Secondly, taking advantage of the perceptually optimized scale resolution the wavelet transform incorporates, a wavelet packet analysis (WPA) scheme is utilized in order to form bands that are close to the Mel - scale. The wavelet transform cepstral coefficients (WTCC) are used as input features while the predicted gains filter each coefficient of the wavelet packet analysis tree. The systems are evaluated using the speech quality and intelligibility metrics at different noise and reverberant environments. While the Fourier transform based system enhances the speech signals closer to VoIP environments it fails to denoise speech signals in situations with negative SNR values or when reverberation is present. On the other hand, the wavelet based system (WaveRNN) performs significantly better in all testing environments.

## Full Report & Presentation (in Greek)

The thesis report can be found [here](https://ikee.lib.auth.gr/record/334884/files/Morogiannis_Apostolos_Speech_Denoising_with_Deep_Learning_2021.pdf), while an interactive presentation whith audio demos can be found [here](https://docs.google.com/presentation/d/1hItovYVjCZXeqGIbo9fMXblWdKtZ3Fm5Sa_C_XB8nRE/edit?usp=sharing).  
The documents are composed in *Greek*, apart from the *Abstract* which is provided above.

# Folder Structure

- The code of the two systems is located in each of the corresponding folders (`FourierRNN`, `WaveRNN`).
- The `evaluation` folder contains the code for the evaluation metrics (PESQ, STOI & NCM), as well as the evaluation results in `.npz` format for each of the used datasets.
- The `Filtered examples` folder contains some filtered samples for each network (better demonstation samples can be found in the presentation [link](https://docs.google.com/presentation/d/1hItovYVjCZXeqGIbo9fMXblWdKtZ3Fm5Sa_C_XB8nRE/edit?usp=sharing)).

# Acknowledgments

The neural network architecture and the methology implemented is partialy derived from the RNNoise project [[1]](#1).  
A demo for can be found [here](https://jmvalin.ca/demo/rnnoise/).

# Libraries Used

- All the code is written in `Python 3`.
- The neural architecture is implemented in `PyTorch`.
- `librosa` library was used for audio processing and Fourier based transforms [[2]](#2).
- For the wavelet based transforms, `pywavelets` was used [[3]](#3).
- The systems were evaluated using the `pysemp` library [[4]](#4).

# References

<a id="1">[1]</a>
J. - M. Valin (2018)
"A hybrid dsp/deep learning approach to real-time full-band speech enhancement."

<a id="2">[2]</a>
Brian McFee (2021)
"librosa/librosa: 0.8.1rc1". Zenodo. doi: 10.5281/zenodo.4782663.

<a id="3">[3]</a>
Gregory R. Lee (2019)
"PyWavelets/pywt: PyWavelets 1.1.1". Zenodo. doi: 10.5281/zenodo.3510098.

<a id="4">[4]</a>
schmiph2 (2020)
"schmiph2/pysepm 0.1". Zenodo. doi: 10.5281/zenodo.4021861.
