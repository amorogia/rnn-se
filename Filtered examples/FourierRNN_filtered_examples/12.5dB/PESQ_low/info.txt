p257_310.wav

PESQ : 
Noise                cafe
SNR (dB)              7.5
Noisy             1.05904
WTCC              1.12675
MFCC              1.11812
filt - noisy    0.0590795
Name: 306, dtype: object

STOI :
Noise           cafe
SNR (dB)         7.5
Noisy       0.803794
WTCC        0.853514
MFCC        0.845787
Name: 306, dtype: object

NCM : 
Noise           cafe
SNR (dB)         7.5
Noisy       0.840083
WTCC        0.946605
MFCC        0.949155 
