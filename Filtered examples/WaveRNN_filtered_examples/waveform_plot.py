import librosa
import librosa.display as dsp
import matplotlib.pyplot as plt
import numpy as np

noisy_path = "PESQ/12.5db/PESQ_low/p257_373_noisy.wav" 
filtered_path = "PESQ/12.5db/PESQ_low/p257_373_filtered.wav" 
clean_path = "PESQ/12.5db/PESQ_low/p257_373_clean.wav" 

noisy, sr = librosa.load(noisy_path)
filtered, sr = librosa.load(filtered_path)
clean, sr = librosa.load(clean_path)

noisy = librosa.util.normalize(noisy)
filtered = librosa.util.normalize(filtered)
clean = librosa.util.normalize(clean)

# Display filtered waveform
librosa.display.waveplot(noisy, sr=sr, color='r',alpha=0.3);
librosa.display.waveplot(filtered, sr=sr, color='b')
plt.show()

# Display clean waveform
plt.plot()
librosa.display.waveplot(clean, sr=sr, color='g')
plt.show()

# Display noisy waveform
plt.plot()
librosa.display.waveplot(noisy, sr=sr, color='r')
plt.show()
