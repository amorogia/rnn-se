p257_373.wav

PESQ : 
Noise              living
SNR (dB)             12.5
Noisy             1.10479
Db - 6            1.20548
Db - 14           1.20175
filt - noisy    0.0969574
Name: 369, dtype: object

STOI :
Noise         living
SNR (dB)        12.5
Noisy       0.935811
Db - 6      0.955807
Db - 14     0.960698
Name: 369, dtype: object

NCM : 
Noise         living
SNR (dB)        12.5
Noisy       0.989014
Db - 6      0.998607
Db - 14            1
