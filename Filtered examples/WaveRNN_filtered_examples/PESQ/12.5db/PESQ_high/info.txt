p232_403.wav

PESQ : 
Noise            living
SNR (dB)           12.5
Noisy           1.37956
Db - 6          3.01259
Db - 14         3.20982
filt - noisy    1.83026
Name: 813, dtype: object

STOI :
Noise         living
SNR (dB)        12.5
Noisy       0.937089
Db - 6      0.964752
Db - 14     0.966386
Name: 813, dtype: object

NCM : 
Noise         living
SNR (dB)        12.5
Noisy       0.947516
Db - 6             1
Db - 14            1
Name: 813, dtype: object
