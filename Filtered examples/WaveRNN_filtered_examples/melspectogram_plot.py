import librosa
import librosa.display as dsp
import matplotlib.pyplot as plt
import numpy as np

noisy_path = "PESQ/12.5db/PESQ_low/p257_373_noisy.wav"
filtered_path = "PESQ/12.5db/PESQ_low/p257_373_filtered.wav"
clean_path = "PESQ/12.5db/PESQ_low/p257_373_clean.wav"

noisy, sr = librosa.load(noisy_path)
filtered, sr = librosa.load(filtered_path)
clean, sr = librosa.load(clean_path)

noisy = librosa.util.normalize(noisy)
filtered = librosa.util.normalize(filtered)
clean = librosa.util.normalize(clean)

clean_spectogram = librosa.feature.melspectrogram(y=clean, sr=sr)
noisy_spectogram = librosa.feature.melspectrogram(y=noisy, sr=sr)
filtered_spectogram = librosa.feature.melspectrogram(y=filtered, sr=sr)

fig, ax = plt.subplots()
S_dB = librosa.power_to_db(clean_spectogram, ref=np.max)
img = librosa.display.specshow(S_dB, x_axis='time',
                         y_axis='mel', sr=sr,
                         fmax=8000, ax=ax)
fig.colorbar(img, ax=ax, format='%+2.0f dB')
ax.set(title='Clean Mel-frequency spectrogram')
plt.show()

plt.plot()
fig, ax = plt.subplots()
S_dB = librosa.power_to_db(noisy_spectogram, ref=np.max)
img = librosa.display.specshow(S_dB, x_axis='time',
                         y_axis='mel', sr=sr,
                         fmax=8000, ax=ax)
fig.colorbar(img, ax=ax, format='%+2.0f dB')
ax.set(title='Noisy Mel-frequency spectrogram')
plt.show()

plt.plot()
fig, ax = plt.subplots()
S_dB = librosa.power_to_db(filtered_spectogram, ref=np.max)
img = librosa.display.specshow(S_dB, x_axis='time',
                         y_axis='mel', sr=sr,
                         fmax=8000, ax=ax)
fig.colorbar(img, ax=ax, format='%+2.0f dB')
ax.set(title='Filtered Mel-frequency spectrogram')
plt.show()

