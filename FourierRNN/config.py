'''Configuration file for audio.py functions.

    ...

    Variables  
    ---------
        sr : int
            Sample rate to load and process audio files.
        n_fft : int
            Length of the windowed signal for STFT calculation.
        hop_length : int
            Number of audio samples between adjacent STFT columns.
        n_mels : int 
            Number of mel coefficients. It is also the number of filtering bands.
            We use this at the feature extraction stage.
        n_delta_coef : int
            Number of delta coefficients to be used in feature extraction phase.
        wavelet : str
            Mother wavelet function to be used.
'''

sr = 48000
n_fft = int(0.02133333333333333333333333333*48000)
hop_length = int(n_fft/2)
n_mels = 24
n_delta_coef=6
wavelet='db6'
