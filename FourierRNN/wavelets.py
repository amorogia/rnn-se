import pywt
import librosa
import numpy as np
import config
import audio
from scipy.fftpack import dct
from scipy.signal import get_window

'''
Dictionary for the wavelet packet spectral bands.
We choose those in order to approach the mel scale spacing. 
'''
mel_wavelet_bands = {
    0: "aaaaaa",
    1: "aaaaad",
    2: "aaaada",
    3: "aaaadd",
    4: "aaadaa",
    5: "aaadad",
    6: "aaadda",
    7: "aaaddd",
    8: "aadaa",
    9: "aadad",
    10: "aadda",
    11: "aaddd",
    12: "adaaa",
    13: "adaad",
    14: "adada",
    15: "adadd",
    16: "addaa",
    17: "addad",
    18: "addd",
    19: "daaa",
    20: "daad",
    21: "dad",
    22: "dda",
    23: "ddd"
}


def WTCC(audio_path):
    '''A funtion to calculate wavelet transform cepstral coefficients.
    Parameters
    ----------
        audio_path : str
            Folder path of an audio file
    Output
    ------
        WTCC : np.ndarray
            Wavelet cepstral coefficients.
    '''

    # Load Signal
    signal, sr = librosa.load(audio_path, sr=config.sr)
    # Normalize Signal
    signal = librosa.util.normalize(signal)
    # Pad signal to match the frames
    signal = np.pad(signal, int(config.n_fft // 2), mode='reflect')
    # Frame signal
    framed_signal = librosa.util.frame(signal,
                                       frame_length=config.n_fft,
                                       hop_length=config.hop_length)

    # Get hann window at n_fft size
    window = get_window("hann", config.n_fft)

    # Initialize wavelet cepstral coefficients array
    WTCC = np.zeros((len(mel_wavelet_bands), framed_signal.shape[1]),
                    dtype='float32')

    # Iterate through frames
    for frame_idx in range(0, framed_signal.shape[1]):
        # Calculate the wavelet packet transform from the specific frame multiplied by the window function
        wp = pywt.WaveletPacket(data=window * framed_signal[:, frame_idx],
                                wavelet=config.wavelet)
        # Iterate through wavelet packet transform bands
        for band_idx in range(0, len(mel_wavelet_bands)):
            # Get band data from the corresponding sequence of the wavelet coefficients
            wavelet_band_data = wp[mel_wavelet_bands[band_idx]].data
            # Square Sequence
            wavelet_band_data = np.square(wavelet_band_data)
            # Calculate Mean
            wavelet_band_data = np.mean(wavelet_band_data)
            # Put Powered scalogram coeff in WTCC array
            WTCC[band_idx, frame_idx] = wavelet_band_data
    # Convert power scalogram to db scalogram.
    WTCC = librosa.power_to_db(WTCC)
    # Compute dct transform along frequency axis.
    WTCC = dct(WTCC, axis=0, type=2, norm="ortho")
    return WTCC


def feature_extraction(clean_audio_file_absolute_path,
                       noisy_audio_file_absolute_path):
    '''This is the main feature extraction function for one file.
        Here we compute the Noisy Wavelet Cepstral Coefficients,
        deltas and deltas-deltas of them. Also we compute the 
        ideal gains and Voice Activity Detection non Linearity.
        
        Parameters 
        ----------
            clean_audio_file_absolute_path : str
                Folder Path for clean audio file.
            noisy_audio_file_absolute_path : str
                Folder Path for noisy audio file.
           
        Returns
        -------
            noisy_WTCC : numpy.ndarray 
                Mel Scaled Wavelet Cepstral Coefficients of the noisy signal.
            WTCC_delta : numpy.ndarray
                WTCCs delta features; A local estimate of the derivative of the 
                input data along the time axis.
            WTCC_delta_delta : numpy.ndarray
                WTCCs delta - delta features; A local estimate of the derivative 
                of the input data along the time axis.
            gb : numpy.ndarray
                Ideal gains for each frequency band.
            VAD : numpy.ndarray 
                This is a non - linearity to specify Voice Activity. It is a 
                binary flag (1 = Voice, 0 = No Voice). 
    '''

    n_delta_coef = config.n_delta_coef

    # Compute mel - filterbanks
    mels = librosa.filters.mel(config.sr,
                               n_fft=config.n_fft,
                               n_mels=config.n_mels,
                               norm=None)

    # Compute ideal gains : gb #
    # Compute STFT of clean and noisy signal
    stft_clean_signal, stft_noisy_signal = audio.load_normalize_stft(
        clean_audio_file_absolute_path, noisy_audio_file_absolute_path)
    # Keep the amplitude of STFT
    S_noisy = np.abs(stft_noisy_signal)
    S_clean = np.abs(stft_clean_signal)
    # Compute energy
    energy_clean = np.matmul(mels, S_clean**2)
    energy_noisy = np.matmul(mels, S_noisy**2)
    # Compute gains
    gb = np.sqrt(energy_clean / energy_noisy)
    # Normalize gains gb to [-1,1]
    gb = librosa.util.normalize(gb)

    # Compute WTCC's for noisy signal #
    noisy_WTCC = WTCC(noisy_audio_file_absolute_path)

    # Compute WTCC deltas #
    #n_delta_coef=6
    WTCC_delta = librosa.feature.delta(noisy_WTCC)
    WTCC_delta = WTCC_delta[:n_delta_coef, :]
    # Compute MFCC deltas - deltas #
    WTCC_delta_delta = librosa.feature.delta(noisy_WTCC, order=2)
    WTCC_delta_delta = WTCC_delta_delta[:n_delta_coef, :]

    # Compute VAD non - linearity : only in the clean file.
    RMS_frame_energy = librosa.feature.rms(S=S_clean,
                                           frame_length=config.n_fft,
                                           hop_length=config.hop_length)
    VAD_threshold = 1e-1
    VAD = (RMS_frame_energy > VAD_threshold).astype(int)

    return noisy_WTCC, WTCC_delta, WTCC_delta_delta, gb, VAD


def gen_dataset(clean_audio_path, noisy_audio_path, logfile_path):
    '''Main function to generate the wavelet features for all the files in the dataset.

        Parameters
        ----------
            clean_audio_path : str
                Folder Path for clean audio files.
            noisy_audio_path : str
                Folder Path for noisy audio files.
            logfile_path : str
                Folder Path for the logfile.
        Returns
        -------
            logdata_list : list
                List with the log file data for each track. Each list entry 
                contains : ['name of the track' 'noise enviroment' 'SNR']
                    e.g : ['p232_005.wav' 'bus' '2.500000e+00']
            noisy_WTCC_arr : numpy.ndarray 
                Wavelet transform cepstral coefficients of the noisy signal.
            WTCC_delta_arr : numpy.ndarray
                WTCCs delta features; A local estimate of the derivative 
                of the input data along the time axis.
            WTCC_delta_delta_arr : numpy.ndarray
                WTCCs delta - delta features; A local estimate of the derivative 
                of the input data along the time axis.
            gb_arr : numpy.ndarray
                Ideal gains for each frequency band.
            VAD_arr : numpy.ndarray 
                This is a non - linearity to specify Voice Activity. It is a 
                binary flag (1 = Voice, 0 = No Voice). 

    '''

    logdata_list = []
    # Input features
    noisy_WTCC_list = []
    WTCC_delta_list = []
    WTCC_delta_delta_list = []
    # Output features
    gb_list = []
    VAD_list = []

    # Open log file and start reading
    logfile = open(logfile_path, "r")

    for line in logfile:
        # Split line attributes
        att = line.split()
        att[0] = att[0] + '.wav'
        audio_file = att[0]
        # Compute absolute paths for one file
        clean_audio_file_absolute_path = (clean_audio_path + audio_file)
        noisy_audio_file_absolute_path = (noisy_audio_path + audio_file)
        # Extract input and output features
        noisy_WTCC, WTCC_delta, WTCC_delta_delta, gb, VAD = feature_extraction(
            clean_audio_file_absolute_path, noisy_audio_file_absolute_path)

        # Append information and features
        logdata_list.append(att)
        # Input features
        noisy_WTCC_list.append(noisy_WTCC)
        WTCC_delta_list.append(WTCC_delta)
        WTCC_delta_delta_list.append(WTCC_delta_delta)
        # Output features
        gb_list.append(gb)
        VAD_list.append(VAD)

    # Trasform ALL lists to numpy array in order to save them
    # Input Features
    noisy_WTCC_arr = transform_list_to_nparray(noisy_WTCC_list)
    WTCC_delta_arr = transform_list_to_nparray(WTCC_delta_list)
    WTCC_delta_delta_arr = transform_list_to_nparray(WTCC_delta_delta_list)
    # Output features
    gb_arr = transform_list_to_nparray(gb_list)
    VAD_arr = transform_list_to_nparray(VAD_list)

    return logdata_list, noisy_WTCC_arr, WTCC_delta_arr, WTCC_delta_delta_arr, gb_arr, VAD_arr


def transform_list_to_nparray(list):
    '''Just a HELPER function for gen_dataset that transforms a list to 
        numpy array of different dimensions.

        Parameters
        ----------
        list : 
            The list to be converted to numpy array.
        Returns
        -------
        arr : 
            The converted numpy array.
    '''

    list_len = len(list)
    arr = np.empty(list_len, object)
    arr[:] = list
    return arr


if __name__ == "__main__":
    clean_audio_path = './example_files/clean/'
    noisy_audio_path = './example_files/noisy/'
    logfile_path = './example_files/log_example.txt'

    # Example of the generated dataset
    log_data, noisy_WTCC, WTCC_delta, WTCC_delta_delta, gb, VAD = gen_dataset(
        clean_audio_path, noisy_audio_path, logfile_path)

    # Save training data
    np.savez("./example_files/generated_dataset/X_wavelets_example.npz",
             noisy_WTCC=noisy_WTCC,
             WTCC_delta=WTCC_delta,
             WTCC_delta_delta=WTCC_delta_delta)
    np.savez("./example_files/generated_dataset/y_example.npz", gb=gb, VAD=VAD)
    np.savez("./example_files/generated_dataset/logdata_example.npz",
             log_data=log_data)

    # LOAD Data
    log_data = np.load("./example_files/generated_dataset/logdata_example.npz",
                       allow_pickle=True)
    att = log_data["log_data"]
    print(att[:, 2])
